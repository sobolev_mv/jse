package ru.nlmk.sobolevmv.tm.comparator;

import ru.nlmk.sobolevmv.tm.entity.Task;
import java.util.Comparator;

public class SortOrderTaskByName  implements Comparator<Task>{

  @Override
  public int compare(Task o1, Task o2) {
    return o1.getName().compareTo(o2.getName());
  }

}
