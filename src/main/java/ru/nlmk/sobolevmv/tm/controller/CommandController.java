package ru.nlmk.sobolevmv.tm.controller;

import ru.nlmk.sobolevmv.tm.entity.Command;
import ru.nlmk.sobolevmv.tm.service.CommandService;
import java.util.Date;

public class CommandController extends AbstractController {

  private final CommandService commandService;

  public CommandController(CommandService commandService) {
    this.commandService = commandService;
  }

  public int createCommand(String nameCommand, Date enterTimeCommand) {
    commandService.create(nameCommand, enterTimeCommand);
    return 0;
  }

  public int clearCommands() {
    System.out.println("[CLEAR COMMAND LIST]");
    commandService.clear();
    System.out.println("[OK]");
    return 0;
  }

  public int listCommands() {
    System.out.println("[LIST COMMANDS]");

    int index = 1;
    for (final Command command: commandService.findAll()) {
      System.out.println(index + ". " + command.getNameCommand() + ": " + command.getEnterTimeCommand().toString());
      index++;
    }
    System.out.println("[OK]");
    return 0;
  }

}
