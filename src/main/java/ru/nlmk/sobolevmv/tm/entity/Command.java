package ru.nlmk.sobolevmv.tm.entity;

import java.util.Date;

public class Command {

  private String nameCommand = "";

  private Date enterTimeCommand = new Date();

  public Command(){}

  public Command(String nameCommand, Date enterTimeCommand){
    this.nameCommand = nameCommand;
    this.enterTimeCommand = enterTimeCommand;
  }

  public String getNameCommand() {
    return nameCommand;
  }

  public void setNameCommand(String nameCommand) {
    this.nameCommand = nameCommand;
  }

  public Date getEnterTimeCommand() {
    return enterTimeCommand;
  }

  public void setEnterTimeCommand(Date enterTimeCommand) {
    this.enterTimeCommand = enterTimeCommand;
  }
}
