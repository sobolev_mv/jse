package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.Command;
import ru.nlmk.sobolevmv.tm.entity.Project;
import ru.nlmk.sobolevmv.tm.repository.CommandRepository;
import ru.nlmk.sobolevmv.tm.repository.ProjectRepository;

import java.util.Date;
import java.util.List;

public class CommandService {

  private final CommandRepository commandRepository;

  public CommandService(final CommandRepository commandRepository) {
    this.commandRepository = commandRepository;
  }

  public Command create(String nameCommand, Date enterTimeCommand) {
    if (nameCommand == null || nameCommand.isEmpty()) return null;
    return commandRepository.create(nameCommand, enterTimeCommand);
  }

  public void clear() {
    commandRepository.clear();
  }

  public List<Command> findAll() {
    return commandRepository.findAll();
  }
}
