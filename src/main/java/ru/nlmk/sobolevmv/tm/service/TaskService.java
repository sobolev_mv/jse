package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.Task;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import ru.nlmk.sobolevmv.tm.repository.TaskRepository;
import java.util.Collections;
import java.util.List;

public class TaskService {

  private final TaskRepository taskRepository;

  public TaskService(final TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  public Task create(final String name) {
    if (name == null || name.isEmpty()) return null;
    return taskRepository.create(name);
  }

  public Task create(final String name, final String description) {
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    return taskRepository.create(name, description);
  }

  public Task update(final Long id, final String name, final String description) throws TaskNotFoundException {
    if (id == null) return null;
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    return taskRepository.update(id, name, description);
  }

  public void clear() {
    taskRepository.clear();
  }

  public Task findByIndex(final int index) throws TaskNotFoundException {
    if(index < 0 || index > taskRepository.getSize()) throw new TaskNotFoundException("Task not Found By Index: " + index);
    return taskRepository.findByIndex(index);
  }

  public Task findByName(final String name) throws TaskNotFoundException {
    if (name == null || name.isEmpty()) return null;
    return taskRepository.findByName(name);
  }

  public List<Task> findAllByName(final String name) {
    if (name == null || name.isEmpty()) return null;
    return taskRepository.findAllByName(name);
  }

  public Task findById(final Long id) throws TaskNotFoundException {
    if (id == null) return null;
    return taskRepository.findById(id);
  }

  public Task removeById(final Long id) throws TaskNotFoundException {
    if (id == null) return null;
    return taskRepository.removeById(id);
  }

  public Task removeByName(final String name) throws TaskNotFoundException {
    if (name == null || name.isEmpty()) return null;
    return taskRepository.removeByName(name);
  }

  public Task removeByIndex(final int index) throws TaskNotFoundException {
    if(index < 0 || index > taskRepository.getSize()) throw new TaskNotFoundException("Task not Found By Index: " + index);
    return taskRepository.removeByIndex(index);
  }

  public List<Task> findAll(Long userId) {
    return taskRepository.findAll(userId);
  }

  public List<Task> findAllByProjectId(final Long projectId, Long userId) {
    if (projectId == null) return Collections.emptyList();
    return taskRepository.findAllByProjectId(projectId, userId);
  }

}
