package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.Project;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.repository.ProjectRepository;
import java.util.List;

public class ProjectService {

  private final ProjectRepository projectRepository;

  public ProjectService(final ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  public Project create(final String name) {
    if (name == null || name.isEmpty()) return null;
    return projectRepository.create(name);
  }

  public Project create(final String name, final String description) {
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    return projectRepository.create(name, description);
  }

  public Project create(final String name, final String description, final Long userId) {
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    return projectRepository.create(name, description, userId);
  }

  public Project update(final Long id, final String name, final String description) throws ProjectNotFoundException {
    if (id == null) return null;
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    return projectRepository.update(id, name, description);
  }

  public void clear() {
    projectRepository.clear();
  }

  public Project findByIndex(final int index) throws ProjectNotFoundException {
    if(index < 0 || index > projectRepository.getSize()) throw new ProjectNotFoundException("Project not Found By Index: " + index);
    return projectRepository.findByIndex(index);
  }

  public Project findByName(final String name) throws ProjectNotFoundException {
    if (name == null || name.isEmpty()) return null;
    return projectRepository.findByName(name);
  }

  public Project findById(final Long id) throws ProjectNotFoundException{
    if (id == null) return null;
    return projectRepository.findById(id);
  }

  public Project removeById(final Long id) throws ProjectNotFoundException {
    if (id == null) return null;
    return projectRepository.removeById(id);
  }

  public Project removeByName(final String name) throws ProjectNotFoundException {
    if (name == null || name.isEmpty()) return null;
    return projectRepository.removeByName(name);
  }

  public Project removeByIndex(final int index) throws ProjectNotFoundException {
    if(index < 0 || index > projectRepository.getSize()) throw new ProjectNotFoundException("Project not Found By Index: " + index);
    return projectRepository.removeByIndex(index);
  }

  public List<Project> findAll(Long userId) {
    return projectRepository.findAll(userId);
  }
}
