package ru.nlmk.sobolevmv.tm.repository;

import ru.nlmk.sobolevmv.tm.entity.Command;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static ru.nlmk.sobolevmv.tm.constant.CommandConst.*;

public class CommandRepository {

  private List<Command> commands = new ArrayList<>(CMD_DEEP);

  public Command create(String nameCommand, Date enterTimeCommand){
    final Command command = new Command(nameCommand, enterTimeCommand);
    if (commands.size() == CMD_DEEP) commands.remove(0);
    commands.add(command);
    return command;
  }

  public void clear(){
    commands.clear();
  }

  public List<Command> findAll()  {
    return commands;
  }

}
