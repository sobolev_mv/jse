package ru.nlmk.sobolevmv.tm.repository;

import ru.nlmk.sobolevmv.tm.entity.Project;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProjectRepository {

  private static final Logger logger = LogManager.getLogger(ProjectRepository.class);

  private final List<Project> projects = new ArrayList<>();

  public Project create(final String name){
    logger.trace("create project name = {}", name);
    final Project project = new Project(name);
    logger.info("create --> {}", project);
    projects.add(project);
    return project;
  }

  public Project create(final String name, final String description) {
    logger.trace("create project name = {} description = {}", name, description);
    final Project project = create(name);
    project.setDescription(description);
    logger.info("create --> {}", project);
    return project;
  }

  public Project create(final String name, final String description, Long userId) {
    logger.trace("create project name = {} description = {} userId = {}", name, description, userId);
    final Project project = create(name, description);
    project.setUserId(userId);
    logger.info("create --> {}", project);
    return project;
  }

  public Project update(final Long id, final String name, final String description) throws ProjectNotFoundException {
    logger.trace("update project id = {} name = {} description = {}", id, name, description);
    final Project project = findById(id);
    if (project == null) return null;
    project.setId(id);
    project.setName(name);
    project.setDescription(description);
    logger.info("update --> {}", project);
    return project;
  }

  public void clear(){
    projects.clear();
  }

  public Project findByIndex(int index) throws ProjectNotFoundException {
    logger.trace("findByIndex project index = {}", index);
    if (index < 0 || index > projects.size() - 1) throw new ProjectNotFoundException("Project not Found By Index: " + index);
    return projects.get(index);
  }

  public Project findByName(final String name) throws ProjectNotFoundException {
    logger.trace("findByName project name = {}", name);
    if (name == null || name.isEmpty()) return null;
    for (final Project project: projects) {
      if (project.getName().equals(name)) return project;
    }
    throw new ProjectNotFoundException("Project not Found By Name: " + name);
  }

  public Project findById(final Long id) throws ProjectNotFoundException {
    logger.trace("findById project id = {}", id);
    if (id == null) return null;
    for (final Project project: projects) {
      if (project.getId().equals(id)) return project;
    }
    throw new ProjectNotFoundException("Project not Found By id: " + id.toString());
  }

  public Project removeById(final Long id) throws ProjectNotFoundException {
    logger.trace("removeById project id = {}", id);
    final Project project = findById(id);
    if (project == null) return null;
    projects.remove(project);
    logger.info("removeById --> {}", project);
    return project;
  }

  public Project removeByName(final String name) throws ProjectNotFoundException {
    logger.trace("removeByName project name = {}", name);
    final Project project = findByName(name);
    if (project == null) return null;
    projects.remove(project);
    logger.info("removeByName --> {}", project);
    return project;
  }

  public Project removeByIndex(final int index) throws ProjectNotFoundException {
    logger.trace("removeByIndex project index = {}", index);
    final Project project = findByIndex(index);
    if (project == null) return null;
    projects.remove(project);
    logger.info("removeByIndex --> {}", project);
    return project;
  }

  public int getSize() {
    return projects.size();
  }

  public List<Project> findAll(Long userId)  {
    logger.trace("findAll projects userId = {}", userId);
    if (userId == null) return Collections.emptyList();
    List<Project> lstTmp = new ArrayList<>();
    for(Project project : projects) {
      if (Objects.equals(project.getUserId(), userId)) lstTmp.add(project);
    }
    return lstTmp;
  }
}

