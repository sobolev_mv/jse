package ru.nlmk.sobolevmv.tm.repository;

import ru.nlmk.sobolevmv.tm.entity.Task;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;

import java.util.*;

public class TaskRepository {

  private final List<Task> tasks = new ArrayList<>();

  private final Map<String, List<Task>> indexTasks = new HashMap<>();

  public Task create(final String name){
    final Task task = new Task(name);
    tasks.add(task);
    
    List<Task> tmpTasks = indexTasks.get(name);
    if (tmpTasks == null) {
      tmpTasks = new ArrayList<>();
      tmpTasks.add(task);
    } else {
      tmpTasks.clear(); 
      indexTasks.remove(name);
      for (final Task tsk: tasks)
        if (tsk.getName().equals(name)) tmpTasks.add(tsk);
    }

    indexTasks.put(name, tmpTasks);
    return task;
  }

  public Task create(final String name, final String description) {
    final Task task = create(name);
    task.setDescription(description);
    return task;
  }

  public Task create(final String name, final String description, Long userId) {
    final Task task = create(name, description);
    task.setUserId(userId);
    return task;
  }

  public Task update(final Long id, final String name, final String description) throws TaskNotFoundException {
    final Task task = findById(id);
    if (task == null) return null;
    task.setId(id);
    task.setName(name);
    task.setDescription(description);
    return task;
  }

  public void clear(){
    tasks.clear();
  }

  public Task findByIndex(final int index) throws TaskNotFoundException {
    if(index < 0 || index > tasks.size() - 1) throw new TaskNotFoundException("Task not Found By Index: " + index);
    return tasks.get(index);
  }

  public List<Task> findAllByName(final String name) {
    if (name == null || name.isEmpty()) return null;
    return indexTasks.get(name);
  }

  public Task findByName(final String name) throws TaskNotFoundException {
    if (name == null || name.isEmpty()) return null;
    for (final Task task: tasks) {
      if (task.getName().equals(name)) return task;
    }
    throw new TaskNotFoundException("Task not Found By Name: " + name);
  }

  public Task findById(final Long id) throws TaskNotFoundException {
    if (id == null) return null;
    for (final Task task: tasks) {
      if (task.getId().equals(id)) return task;
    }
    throw new TaskNotFoundException("Task not Found By Id: " + id.toString());
  }

  public Task removeById(final Long id) throws TaskNotFoundException {
    final Task task = findById(id);
    if (task == null) return null;
    tasks.remove(task);
    return task;
  }

  public Task removeByName(final String name) throws TaskNotFoundException {
    final Task task = findByName(name);
    if (task == null) return null;
    tasks.remove(task);
    return task;
  }

  public Task removeByIndex(final int index) throws TaskNotFoundException {
    final Task task = findByIndex(index);
    if (task == null) return null;
    tasks.remove(task);
    return task;
  }

  public int getSize() {
    return tasks.size();
  }

  public List<Task> findAllByProjectId(final Long projectId, Long userId){
    final List<Task> result = new ArrayList<>();
    for (final Task task: findAll(userId)){
      final Long idProject = task.getProjectId();
      if (idProject == null) continue;
      if (idProject.equals(projectId)) result.add(task);
    }
    return result;
  }

  public Task findByProjectIdAndId(final Long projectId, final Long id) {
    if (id == null) return null;
    for (final Task task: tasks) {
      final Long idProject = task.getProjectId();
      if (idProject == null) continue;
      if (!idProject.equals(projectId)) continue;
      if (task.getId().equals(id)) return task;
    }
    return null;
  }

  public List<Task> findAll(Long userId){
    if (userId == null) return Collections.emptyList();
    List<Task> lstTmp = new ArrayList<>();
    for(Task task : tasks) {
      if (Objects.equals(task.getUserId(), userId)) lstTmp.add(task);
    }
    return lstTmp;
  }
}

